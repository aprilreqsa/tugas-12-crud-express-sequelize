//import model
const { venues} = require('../models')

class VenuesController {
    static async findAll(req, res){
        let Venues = await venues.findAll()
        res.status(200).json({status : 'success', data : Venues})
    }
    static async store(req, res){
        let name = req.body.name
        let address = req.body.address
        let phone = req.body.phone
        const Venues1 = await venues.create({name: name, address: address,phone:phone})
        res.status(200).json({status: 'success', message: 'saved!'})
    }
    static async show(req, res){
        let id = req.params.id
        let venue = await venues.findByPk(id)
        res.status(200).json({status: 'success', data: venue})
    }
    static async update(req, res){
        let name = req.body.name
        let address = req.body.address
        let phone = req.body.phone
        await venues.update({
            name: name,
            address: address,
            phone: phone
        }, {
            where : {
                id: req.params.id
            }
        })
        res.status(200).json({status: 'success', message: 'updated!'})
    }
    static async destroy(req, res){
        await venues.destroy({
            where : {
                id: req.params.id   
            }
        })
        res.status(200).json({status: 'success', message: 'data telah terhapus!'})
    }
}
module.exports = VenuesController