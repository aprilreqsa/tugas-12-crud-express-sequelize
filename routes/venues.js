var express = require('express');
var router = express.Router();
const VenuesController = require('../controllers/venues')

router.get('/', VenuesController.findAll);
router.post('/', VenuesController.store)
router.get('/:id', VenuesController.show)
router.put('/:id', VenuesController.update)
router.delete('/:id', VenuesController.destroy)
module.exports = router;